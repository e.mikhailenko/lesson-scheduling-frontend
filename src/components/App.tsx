import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { ChakraProvider, extendTheme, useColorMode, useDisclosure } from '@chakra-ui/react';
import store from '../redux/store';
import Header from './Header';
import LessonList from './LessonList';
import SchedulePage from '../pages/SchedulePage';
import StudentsPage from '../pages/StudentsPage';
import TeachersPage from '../pages/TeachersPage';
import StatisticsPage from '../pages/StatisticsPage';
import SideBar from '../widgets/SideBar';

const App: React.FC = () => {
  const sidebar = useDisclosure();
  const { colorMode } = useColorMode();
  const theme = extendTheme({
    config: {
      initialColorMode: colorMode === 'light' ? 'dark' : 'light',
      useSystemColorMode: false,
    },
  });

  return (
    <Provider store={store}>
      <ChakraProvider theme={theme}>
        <Router>
          <SideBar
            display={{
              base: "none",
              md: "unset",
            }}
          />
          <Header />
          <Routes>
            <Route path="/schedule" element={<SchedulePage />} />
            <Route path="/students" element={<StudentsPage />} />
            <Route path="/teachers" element={<TeachersPage />} />
            <Route path="/lessons" element={<LessonList />} />
            <Route path='/statistics' element={<StatisticsPage />} />
          </Routes>
        </Router>
      </ChakraProvider>
    </Provider>
  );
};

export default App;
