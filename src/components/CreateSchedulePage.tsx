import React, { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { fetchStudents } from '../redux/actions/studentActions';
import { fetchTeachers } from '../redux/actions/teacherActions';
import { fetchLessons } from '../redux/actions/lessonActions';
import { Alert, AlertDescription, AlertIcon, AlertTitle, Box, Button, CloseButton, HStack } from '@chakra-ui/react';
import { createSchedule } from '../redux/actions/scheduleActions';
import { Select } from 'chakra-react-select';
import CreateScheduleModal from '../widgets/CreateScheduleModal';

const ScheduleCreatePage: React.FC = () => {
  const dispatch = useAppDispatch();
  const students = useAppSelector((state) => state.students?.students);
  const teachers = useAppSelector((state) => state.teachers?.teachers);
  const lessons = useAppSelector((state) => state.lessons?.lessons);
  const [selectedStudents, setSelectedStudents] = useState<any>(null);
  const [selectedLessons, setSelectedLessons] = useState<any>(null);
  const [selectedTeacher, setSelectedTeacher] = useState<any>();
  const [alert, setAlert] = useState<{ status: 'success' | 'error'; message: string } | null>(null);

  useEffect(() => {
    dispatch(fetchStudents());
    dispatch(fetchTeachers());
    dispatch(fetchLessons());
  }, [dispatch]);

  const handleSelectStudent = (selectedOptions: any) => {
    if (selectedOptions.length <= 3) {
      setSelectedStudents(selectedOptions || []);
    } else {
      setAlert({ status: 'error', message: 'You can select up to 3 students only.' });
    }
  };

  const handleSelectLesson = (selectedOptions: any) => {
    if (selectedOptions.length <= 2) {
      setSelectedLessons(selectedOptions || []);
    } else {
      setAlert({ status: 'error', message: 'You can select up to 2 lessons only.' });
    }
  };

  const handleSubmit = async () => {
    const studentsIds = selectedStudents?.map((item: any) => item?.value);
    const teacherId = selectedTeacher?.value;
    const lessonsIds = selectedLessons?.map((item: any) => item?.value);
    if (studentsIds && teacherId && lessonsIds) {
      try {
        dispatch(await createSchedule(studentsIds, teacherId, lessonsIds));
        setAlert({ status: 'success', message: 'Schedule created successfully!' });
      } catch (error) {
        setAlert({ status: 'error', message: 'Failed to create schedule. Please try again.' });
      }
    } else {
      setAlert({ status: 'error', message: 'Please select a teacher, at least one student, and at least one lesson.' });
    }
  };

  const studentOptions = students.map((student: any) => ({ value: student.id, label: student.name }));
  const lessonOptions = lessons.map((lesson: any) => ({ value: lesson.id, label: lesson.name }));
  const teacherOptions = teachers.map((teacher: any) => ({ value: teacher.id, label: teacher.name }));

  return (
    <Box p={4} alignItems={'center'} textAlign={'center'} marginBottom={'30px'}>
      <Box fontSize={'xx-large'} marginBottom={'20px'}>Create Schedule</Box>
      {alert && (
        <Alert status={alert.status} mb={4} justifyContent={'center'}>
          <AlertIcon />
          <AlertTitle>{alert.status === 'success' ? 'Success!' : 'Error!'}</AlertTitle>
          <AlertDescription>{alert.message}</AlertDescription>
          <CloseButton position="absolute" right="8px" top="8px" onClick={() => setAlert(null)} />
        </Alert>
      )}
      <HStack spacing={10}>
        <Box w="40%">
          <Select
            options={teacherOptions}
            onChange={setSelectedTeacher}
            value={selectedTeacher}
            placeholder="Select a teacher"
            chakraStyles={{
              control: (provided) => ({
                ...provided,
                colorScheme: 'teal',
              }),
            }}
          />
        </Box>
        <Box w="30%">
          <Select
            isMulti
            options={studentOptions}
            onChange={handleSelectStudent}
            value={selectedStudents}
            placeholder="Select students (max 3)"
            chakraStyles={{
              control: (provided) => ({
                ...provided,
                colorScheme: 'teal',
              }),
            }}
          />
        </Box>
        <Box w="30%">
          <Select
            isMulti
            options={lessonOptions}
            onChange={handleSelectLesson}
            value={selectedLessons}
            placeholder="Select lessons (max 2)"
            chakraStyles={{
              control: (provided) => ({
                ...provided,
                colorScheme: 'teal',
              }),
            }}
          />
        </Box>
      </HStack>
      <Button mt={4} colorScheme="teal" onClick={handleSubmit}>
        Create Schedule
      </Button>
    </Box>
  );
};

export default ScheduleCreatePage;