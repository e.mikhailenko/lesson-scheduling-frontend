import React from 'react';
import { Box, Button, Flex, HStack, Spacer, useColorMode, useColorModeValue } from '@chakra-ui/react';
import { NavLink } from 'react-router-dom';
import { FaMoon, FaSun } from 'react-icons/fa';

const Header: React.FC = () => {
    const { toggleColorMode } = useColorMode();
    const icon = useColorModeValue(<FaMoon />, <FaSun />);

    return (
        <Box bg={useColorModeValue('gray.100', 'gray.900')} px={4}>
            <Flex h={16} alignItems="center" justifyContent="space-between">
                {/* <Box>School Scheduler</Box> */}
                <Spacer />
                <Flex alignItems="centers">
                    {/* <HStack as="nav" spacing={4}>
                        <Button as={NavLink} to="/schedule" colorScheme="teal" variant="ghost">
                            Schedule
                        </Button>
                        <Button as={NavLink} to="/students" colorScheme="teal" variant="ghost">
                            Students
                        </Button>
                        <Button as={NavLink} to="/teachers" colorScheme="teal" variant="ghost">
                            Teachers Working Hours
                        </Button>
                        <Button as={NavLink} to="/statistics" colorScheme="teal" variant="ghost">
                            Teachers statistics
                        </Button>
                    </HStack> */}
                    <Button onClick={toggleColorMode} ml={4}>
                        {icon}
                    </Button>
                </Flex>
            </Flex>
        </Box>
    );
};

export default Header;