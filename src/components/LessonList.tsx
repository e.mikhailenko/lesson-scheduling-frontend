import React, { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { fetchLessons } from '../redux/actions/lessonActions';
import { Box, List, ListItem, Spinner } from '@chakra-ui/react';

const LessonList: React.FC = () => {
  const dispatch = useAppDispatch();
  const { lessons, error } = useAppSelector((state) => state.lessons);

  useEffect(() => {
    dispatch(fetchLessons());
  }, [dispatch]);

  if (error) {
    return <Box p={4}>Error: {error}</Box>;
  }

  if (!lessons.length) {
    return (
      <Box p={4}>
        <Spinner />
      </Box>
    );
  }

  return (
    <Box p={4}>
      <h1>Lessons</h1>
      <List spacing={3}>
        {lessons.map((lesson: any) => (
          <ListItem key={lesson.id}>{lesson.name}</ListItem>
        ))}
      </List>
    </Box>
  );
};

export default LessonList;