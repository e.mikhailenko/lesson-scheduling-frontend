import { As, Flex, Icon } from "@chakra-ui/react"
import { ReactElement } from "react"

interface NavBarProps {
  icon?: As
  children: ReactElement
}

const NavItem = (props: NavBarProps) => {
  const { icon, children, ...restProps } = props
  return (
    <Flex
      align={'center'}
      px={4}
      pl={4}
      py={3}
      cursor={'pointer'}
      role={'group'}
      {...restProps}  
    >
      {icon && (
        <Icon
          mx="2"
          boxSize="4"
          as={icon}
        />
      )}
      {children}
    </Flex>
  )
}

export default NavItem