import React, { useEffect, useState } from 'react';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { fetchSchedule } from '../redux/actions/scheduleActions';
import { Box, Button, Stack, Skeleton, Table, Tbody, Td, Th, Thead, Tr, Text } from '@chakra-ui/react';
import TeachersSelector from '../widgets/TeachersSelector';
import { lessonsNamesMap } from '../const';

const ScheduleTable: React.FC = () => {
  const dispatch = useAppDispatch();
  const { schedule, isLoading } = useAppSelector((state) => state.schedule);
  const [filterTeachers, setTeachers] = useState<any>(undefined)

  // get all unique time slots, days and teachers
  // @ts-ignore
  const timeSlots = [...new Set(schedule.map((item: any) => item.time_slot))];
  const daysOfWeek = ['月', '火', '水', '木', '金'];
  // @ts-ignore
  const teachers = [...new Set(schedule.map((item: any) => item.teacher.name))];

  // sort time slots and days of week
  timeSlots.sort((a, b) => {
    const aTime = a.split(':').map(Number);
    const bTime = b.split(':').map(Number);
    return aTime[0] - bTime[0] || aTime[1] - bTime[1];
  });

  useEffect(() => {
    dispatch(fetchSchedule(filterTeachers?.map((teacher: any) => teacher.value)));
  }, [dispatch, filterTeachers]);

  const getDayOfWeek: { [key: string]: string } = {
    'monday': '月',
    'tuesday': '火',
    'wednesday': '水',
    'thursday': '木',
    'friday': '金'
  }

  return (
    <Box p={4} alignItems={'center'} alignContent={'center'} alignSelf={'center'} textAlign={'center'}>
      <Box display='flex' flexDirection={'row'} alignContent={'center'} mb={10}>
        <Text fontSize={'xl'} mr={10}>フィルター: </Text>
        <TeachersSelector isMulti value={filterTeachers} onChange={setTeachers} />
      </Box>
      {isLoading ? (
        <Stack>
          <Skeleton height='20px' />
          <Skeleton height='20px' />
          <Skeleton height='20px' />
        </Stack>
      ) : (
        <>
          {
            teachers.map(teacher => {
              const scheduleForTeacher = schedule.filter((s: any) => s.teacher.name === teacher);
              // @ts-ignore
              const students = [...new Set(scheduleForTeacher.map(s => s.student.name))];
              return (
                <Box key={teacher} marginBottom={20}>
                  <Button marginBottom={5} fontSize={'x-large'} colorScheme="teal" variant="ghost">{teacher} ({students.join(', ')})</Button>
                  <Table variant='striped' size={'m'}>
                    <Thead>
                      <Tr>
                        <Th textAlign="center" verticalAlign="middle">時間</Th>
                        {daysOfWeek.map(day => <Th key={day} textAlign="center" verticalAlign="middle">{day}</Th>)}
                      </Tr>
                    </Thead>
                    <Tbody>
                      {timeSlots.map(timeSlot => (
                        <Tr key={timeSlot}>
                          <Td textAlign="center" verticalAlign="middle">{timeSlot}</Td>
                          {daysOfWeek.map(day => {
                            const items = scheduleForTeacher.filter((i: any) => getDayOfWeek[i.day_of_week] === day && i.time_slot === timeSlot);

                            if (items.length > 0) {
                              // @ts-ignore
                              return (
                                <Td key={`${day}-${timeSlot}`} textAlign="center" verticalAlign="middle">
                                  {items.map((item: any) => (
                                    // @ts-ignore
                                    <p key={`${day}-${timeSlot}-${item.lesson.name}-${item.student.name}`}>{lessonsNamesMap[item.lesson.name]} ({item.student.name})</p>
                                  ))}
                                </Td>
                              )
                            } else {
                              return <Td key={`${day}-${timeSlot}`} textAlign="center" verticalAlign="middle">---</Td>
                            }
                          })}
                        </Tr>
                      ))}
                    </Tbody>
                  </Table>
                </Box>
              )
            })
          }
        </>
      )
      }
    </Box>
  );
};

export default ScheduleTable;