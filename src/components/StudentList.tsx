import React, { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { fetchStudents } from '../redux/actions/studentActions';
import { Box, Stack, Skeleton, Table, Tbody, Td, Th, Thead, Tr } from '@chakra-ui/react';
import { Student } from '../types';

const StudentList: React.FC = () => {
  const dispatch = useAppDispatch();
  let { students, isLoading } = useAppSelector((state) => state.students);
  students = students.sort((a: Student, b: Student) => {
    let fa = a.name.toLowerCase();
    let fb = b.name.toLowerCase();

    if (fa < fb) {
      return -1;
    }
    if (fa > fb) {
      return 1;
    }
    return 0;
  });

  useEffect(() => {
    dispatch(fetchStudents());
  }, [dispatch]);

  return (
    <Box p={4}>
      {isLoading ? (
        <Stack>
          <Skeleton height='20px' />
          <Skeleton height='20px' />
          <Skeleton height='20px' />
        </Stack>
      ) : (
        <Table variant={'simple'} size={'large'}>
          <Thead>
            <Tr>
              <Th>児童</Th>
              <Th>年齢</Th>
              <Th>日本語レベル</Th>
            </Tr>
          </Thead>
          <Tbody>
            {students.map((student: Student) => (
              <Tr key={student.id}>
                <Td>{student.name}</Td>
                <Td>{student.age}</Td>
                <Td>{student.japanese_level}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      )}
    </Box>
  );
};

export default StudentList;
