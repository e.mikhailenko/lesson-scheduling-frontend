import React, { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { fetchTeachers } from '../redux/actions/teacherActions';
import { Box, Stack, Skeleton, Table, Tbody, Td, Th, Thead, Tr } from '@chakra-ui/react';
import { Teacher } from '../types';

const TeacherList: React.FC = () => {
  const dispatch = useAppDispatch();
  let { teachers, isLoading } = useAppSelector((state) => state.teachers);

  useEffect(() => {
    dispatch(fetchTeachers());
  }, [dispatch]);

  return (
    <Box p={4}>
      {isLoading ? (
        <Stack>
          <Skeleton height='20px' />
          <Skeleton height='20px' />
          <Skeleton height='20px' />
        </Stack>
      ) : (
        <Table variant={'striped'} size={'large'}>
          <Thead>
            <Tr>
              <Th>教師</Th>
              <Th>月</Th>
              <Th>火</Th>
              <Th>水</Th>
              <Th>木</Th>
              <Th>金</Th>
            </Tr>
          </Thead>
          <Tbody>
            {teachers.map((teacher: Teacher) => (
              <Tr key={teacher.id}>
                <Td>{teacher.name}</Td>
                <Td>{teacher.working_hours.monday.join(', ')}</Td>
                <Td>{teacher.working_hours.tuesday.join(', ')}</Td>
                <Td>{teacher.working_hours.wednesday.join(', ')}</Td>
                <Td>{teacher.working_hours.thursday.join(', ')}</Td>
                <Td>{teacher.working_hours.friday.join(', ')}</Td>
              </Tr>
            ))}
          </Tbody>
        </Table>
      )}
    </Box>
  );
};

export default TeacherList;