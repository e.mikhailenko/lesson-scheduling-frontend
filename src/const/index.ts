export const HOST = process.env.REACT_APP_HOST;

export const lessonsNamesMap = {
  japanese: '国語',
  mathematics: '算数',
  music: '音楽',
  science: '生活'
}