import { useEffect } from "react";
import { useAppDispatch, useAppSelector } from "../redux/hooks";
import { Schedule, StatisticData, TeachersTotalWorkingHours, TeachersWorkingHourPerWeek } from "../types";
import { fetchTeachers } from "../redux/actions/teacherActions";
import { fetchSchedule } from "../redux/actions/scheduleActions";

function calculateWorkingHours(workingHours: any) {
  let hours: {[key: string]: number} = {}
  for (let day in workingHours) {
      hours[day] = 0;
      workingHours[day].forEach((period: any) => {
          let [start, end] = period.split('-').map((time: any) => time.split(':').map(Number));
          let startHour = start[0] + start[1] / 60;
          let endHour = end[0] + end[1] / 60;
          hours[day] += endHour - startHour;
      });
  }
  return hours;
}

export const useStatisticData = (): StatisticData => {
  const dispatch = useAppDispatch()
  const { schedule, isLoading } = useAppSelector((state) => state.schedule)
  let teachersData: {[key: number]: TeachersWorkingHourPerWeek} = {};

  useEffect(() => {
    dispatch(fetchSchedule())
  }, [dispatch])

  schedule.forEach((entry: Schedule) => {
    const teacher = entry.teacher;
    if (!teachersData[teacher.id]) {
        teachersData[teacher.id] = {
            name: teacher.name,
            workingHours: calculateWorkingHours(teacher.working_hours)
        };
    }
  })

  const  getTeacherTotalHours = (): TeachersTotalWorkingHours[] => {
    return Object.values(teachersData).map((teacher: TeachersWorkingHourPerWeek) => {
      const totalHours = Object.values(teacher.workingHours).reduce((acc, hours) => acc + hours, 0);
      return {
        teacherId: teacher.teacherId,
        name: teacher.name,
        hours: totalHours
      }
    })
  }
  
  return {
    rawData: schedule,
    teachersWorkingOurPerWeek: teachersData,
    teacherTotalWorkingHours: getTeacherTotalHours()
  }
}