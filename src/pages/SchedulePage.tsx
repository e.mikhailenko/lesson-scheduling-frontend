import { Box, Button, Text } from "@chakra-ui/react"
import ScheduleTable from "../components/ScheduleTable"
import CreateScheduleModal from "../widgets/CreateScheduleModal"
import { useState } from "react"
import { useAppDispatch } from "../redux/hooks"
import { fetchSchedule } from "../redux/actions/scheduleActions"

/* 
  Title: Weekly schedule
  Btn: 
*/
const SchedulePage = () => {
  const dispatch = useAppDispatch()
  const [isCreateScheduleModalOpen, setIsCreateScheduleModalOpen] = useState<boolean>(false)

  const updateSchedule = () => {
    dispatch(fetchSchedule())
  }

  return (
    <Box 
      p='16'
      ml={{
        base: 0,
        md: 60,
      }}
      transition=".3s ease"
      as="main"
    >
      <Box display='flex' alignItems='baseline' justifyContent='space-between'>
        <Text fontSize='4xl' marginBottom={5}>週間スケジュール</Text>
        <Button onClick={() => {setIsCreateScheduleModalOpen(true)}}>スケジュールを作成する</Button>
      </Box>
      <ScheduleTable/>
      <CreateScheduleModal 
        isOpen={isCreateScheduleModalOpen} 
        onClose={() => {setIsCreateScheduleModalOpen(false)}}
        onScheduleCreated={updateSchedule}
      />
    </Box>
  )
}

export default SchedulePage