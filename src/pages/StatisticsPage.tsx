import { Box, Text } from "@chakra-ui/react"
import TeacherTotalWorkingHoursPieChart from "../widgets/TeacherTotalWorkingHoursPieChart"
import LineGraphWorkingHours from "../widgets/LineGraph"

/* Title: statistic */
const StatisticPage = () => {
  return (
    <Box
      p='16'
      ml={{
        base: 0,
        md: 60,
      }}
      transition=".3s ease"
      as="main"
    >
      <Box display='flex' alignItems='baseline' mb={8}>
        <Text fontSize='4xl'>統計</Text>
      </Box>
      <Box display='flex' flexDirection='row' alignContent='center' justifyContent='center'>
        <Box w='50%' mr={10}>
          <TeacherTotalWorkingHoursPieChart />
        </Box>
        <Box w='50%' alignContent='center'>
          <LineGraphWorkingHours />
        </Box>
      </Box>
    </Box>
  )
}

export default StatisticPage