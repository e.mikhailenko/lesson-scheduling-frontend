import { Box, Text } from "@chakra-ui/react"
import StudentList from "../components/StudentList"

/*Title: students table*/
const StudentsPage = () => {
  return (
    <Box
      p='16'
      ml={{
        base: 0,
        md: 60,
      }}
      transition=".3s ease"
      as="main">
      <Box display='flex' alignItems='baseline' mb={8}>
        <Text fontSize='4xl'>児童のデータ表 </Text>
      </Box>
      <StudentList />
    </Box>
  )
}

export default StudentsPage