import { Box, Text } from "@chakra-ui/react"
import TeacherList from "../components/TeacherList"

/* Title: teachers working hours */
const TeachersPage = () => {
  return (
    <Box
      p='16'
      ml={{
        base: 0,
        md: 60,
      }}
      transition=".3s ease"
      as="main"
    >
      <Box display='flex' alignItems='baseline' mb={8}>
        <Text fontSize='4xl'>教師の勤務時間</Text>
      </Box>
      <TeacherList />
    </Box>
  )
}

export default TeachersPage