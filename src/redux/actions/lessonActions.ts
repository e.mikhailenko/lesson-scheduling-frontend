import { Dispatch } from 'redux';
import axios from 'axios';
import { Lesson } from '../../types';
import { HOST } from '../../const';

export const fetchLessons = () => async (dispatch: Dispatch) => {
  dispatch({ type: 'FETCH_LESSONS_REQUEST' })
  try {
    const response = await axios.get<Lesson[]>(`${HOST}/lesson/list`);
    dispatch({ type: 'FETCH_LESSONS_SUCCESS', payload: response.data });
  } catch (error) {
    // @ts-ignore
    dispatch({ type: 'FETCH_LESSONS_FAILURE', payload: error.message });
  }
};