import { Dispatch } from 'redux';
import axios from 'axios';
import { Schedule } from '../../types';
import { HOST } from '../../const';

export const fetchSchedule = (teacherIds?: number[]) => async (dispatch: Dispatch) => {
  dispatch({ type: 'FETCH_SCHEDULE_REQUEST' })
  try {
    const teachersIdsQuery = teacherIds && teacherIds.length > 0 ? `?teacherIds=[${teacherIds.join(',')}]` : ''
    const response = await axios.get<Schedule[]>(`${HOST}/schedule/list${teachersIdsQuery}`);
    dispatch({ type: 'FETCH_SCHEDULE_SUCCESS', payload: response.data });
  } catch (error) {
    // @ts-ignore
    dispatch({ type: 'FETCH_SCHEDULE_FAILURE', payload: error.message });
  }
};

export const createSchedule = (studentIds: number[], teacherId: number | undefined, lessonIds: number[]) => async (dispatch: Dispatch) => {
  dispatch({ type: 'CREATE_SCHEDULE_REQUEST' })
  try {
    const response = await axios.post(`${HOST}/schedule/create`, {
      studentIds,
      teacherId,
      lessonIds,
    });
    console.log('created schedule: ', response.data);
    dispatch({ type: 'CREATE_SCHEDULE_SUCCESS', payload: response.data });
  } catch (error) {
    console.error('Failed to create schedule: ', error);
    dispatch({ type: 'CREATE_SCHEDULE_FAILURE', payload: (error as Error).message });
  }
}