import {Dispatch} from 'redux';
import axios from 'axios';
import {Student} from '../../types';
import {HOST} from "../../const";

export const fetchStudents = () => async (dispatch: Dispatch) => {
    dispatch({ type: 'FETCH_STUDENTS_REQUEST' })
    try {
        const response = await axios.get<Student[]>(`${HOST}/student/list`);
        dispatch({type: 'FETCH_STUDENTS_SUCCESS', payload: response.data});
    } catch (error) {
        // @ts-ignore
        dispatch({type: 'FETCH_STUDENTS_FAILURE', payload: error.message});
    }
};