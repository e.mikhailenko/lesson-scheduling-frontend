import { Dispatch } from 'redux';
import axios from 'axios';
import { Teacher } from '../../types';
import {HOST} from "../../const";

export const fetchTeachers = () => async (dispatch: Dispatch) => {
    dispatch({ type: 'FETCH_TEACHERS_REQUEST' })
    try {
        const response = await axios.get<Teacher[]>(`${HOST}/teacher/list`);
        dispatch({ type: 'FETCH_TEACHERS_SUCCESS', payload: response.data });
    } catch (error) {
        // @ts-ignore
        dispatch({ type: 'FETCH_TEACHERS_FAILURE', payload: error.message });
    }
};