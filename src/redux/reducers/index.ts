import { combineReducers } from '@reduxjs/toolkit';
import studentReducer from './studentReducer';
import teacherReducer from './teacherReducer';
import scheduleReducer from './scheduleReducer';
import lessonReducer from './lessonReducer';

const rootReducer = combineReducers({
    students: studentReducer,
    teachers: teacherReducer,
    schedule: scheduleReducer,
    lessons: lessonReducer,
});

export default rootReducer;