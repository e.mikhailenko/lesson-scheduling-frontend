const initialState = {
  lessons: [],
  isLoading: false,
  error: null,
};

const lessonReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case 'FETCH_LESSONS_REQUEST': 
      return { ...state, isLoading: true}
    case 'FETCH_LESSONS_SUCCESS':
      return { ...state, lessons: action.payload, isLoading: false };
    case 'FETCH_LESSONS_FAILURE':
      return { ...state, error: action.payload, isLoading: false };
    default:
      return state;
  }
};

export default lessonReducer;