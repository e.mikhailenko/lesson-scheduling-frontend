const initialState = {
    schedule: [],
    isLoading: false,
    error: null,
};

const scheduleReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case 'FETCH_SCHEDULE_REQUEST':
        case 'CREATE_SCHEDULE_REQUEST':
            return { ...state, isLoading: true };
        case 'FETCH_SCHEDULE_SUCCESS':
            return { ...state, schedule: action.payload, isLoading: false };
        case 'FETCH_SCHEDULE_FAILURE':
            return { ...state, error: action.payload, isLoading: false };
        case 'CREATE_SCHEDULE_SUCCESS':
            return { ...state, schedule: action.payload };
        case 'CREATE_SCHEDULE_FAILURE':
            return { ...state, error: action.payload };
        default:
            return state;
    }
};

export default scheduleReducer;