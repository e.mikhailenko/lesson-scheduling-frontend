const initialState = {
    students: [],
    isLoading: false,
    error: null,
};

const studentReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case 'FETCH_STUDENTS_REQUEST': 
            return { ...state, isLoading: true }
        case 'FETCH_STUDENTS_SUCCESS':
            return { ...state, students: action.payload, isLoading: false };
        case 'FETCH_STUDENTS_FAILURE':
            return { ...state, error: action.payload };
        default:
            return state;
    }
};

export default studentReducer;