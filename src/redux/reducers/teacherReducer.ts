const initialState = {
    teachers: [],
    isLoading: false,
    error: null,
};

const teacherReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case 'FETCH_TEACHERS_REQUEST':
            return { ...state, isLoading: true }
        case 'FETCH_TEACHERS_SUCCESS':
            return { ...state, teachers: action.payload, isLoading: false };
        case 'FETCH_TEACHERS_FAILURE':
            return { ...state, error: action.payload, isLoading: false };
        default:
            return state;
    }
};

export default teacherReducer;