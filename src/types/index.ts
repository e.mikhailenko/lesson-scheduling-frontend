export interface Student {
  id: number;
  name: string;
  age: number;
  japanese_level: number;
}

export interface Teacher {
  id: number;
  name: string;
  working_hours: { [key: string]: string[] };
}

export interface Lesson {
  id: number;
  name: string;
}

export interface Schedule {
  teacher_id: number;
  teacher: {id: number, name: string, working_hours: any[]},
  student_id: number;
  lesson_id: number;
  day_of_week: 'Monday' | 'Tuesday' | 'Wednesday' | 'Thursday' | 'Friday';
  time_slot: string;
}

export interface RootState {
  students: {
    students: Student[];
    error: string | null;
  };
  teachers: {
    teachers: Teacher[];
    error: string | null;
  };
  schedule: {
    schedule: Schedule[];
    error: string | null;
  };
}

export interface TeachersWorkingHourPerWeek {
  teacherId?: number
  name?: string
  workingHours: {[key: string]: number};
}

export interface TeachersTotalWorkingHours {
  teacherId?: number
  name?: string
  hours?: number
}

export interface StatisticData {
  rawData: any
  teachersWorkingOurPerWeek: {[key: number]: TeachersWorkingHourPerWeek}
  teacherTotalWorkingHours: TeachersTotalWorkingHours[]
} 