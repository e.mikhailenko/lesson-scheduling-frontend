import { Button, FormControl, FormErrorMessage, FormLabel, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, useToast } from "@chakra-ui/react"
import TeachersSelector from "./TeachersSelector"
import StudentsSelector from "./StudentsSelector"
import LessonOptionsSelector from "./LessonOptionsSelector"
import { useEffect, useState } from "react"
import { useAppDispatch, useAppSelector } from "../redux/hooks"
import { createSchedule } from "../redux/actions/scheduleActions"

interface CreateScheduleProps {
  isOpen: boolean
  onClose: () => void
  onScheduleCreated: () => void
}

const CreateScheduleModal = ({ isOpen, onClose, onScheduleCreated }: CreateScheduleProps) => {
  const toast = useToast()
  const dispatch = useAppDispatch()
  const { isLoading } = useAppSelector((state) => state.schedule);
  const [selectedTeacher, setSelectedTeacher] = useState<any>(null)
  const [selectedStudents, setSelectedStudents] = useState<any>(null)
  const [selectedLessons, setSelectedLessons] = useState<any>(null)

  const [errors, setErrors] = useState<{[key: string]: boolean}>({})

  useEffect(() => {
    setSelectedTeacher(null)
    setSelectedStudents(null)
    setSelectedLessons(null)
    setErrors({})
  }, [isOpen])

  const validate = (): boolean => {
    const validationErrors: { [key: string]: boolean } = {}
    let isValid = true
    if(selectedStudents.length > 3) {
      validationErrors['students'] = true
      isValid = false
    }
    if(selectedLessons.length > 2) {
      validationErrors['lessons'] = true
      isValid = false
    }

    setErrors(validationErrors)
    return isValid
  }

  const handleSubmit = async () => {
    const isValid = validate()
    if(!isValid) return

    const studentsIds = selectedStudents?.map((item: any) => item?.value)
    const teacherId = selectedTeacher[0]?.value
    const lessonsIds = selectedLessons?.map((item: any) => item?.value)

    if (studentsIds && teacherId && lessonsIds) {
      try {
        await dispatch(createSchedule(studentsIds, teacherId, lessonsIds))
        toast({
          title: 'スケジュールが作成されました。',
          description: "スケジュールが正常に追加されました。",
          status: 'success',
          position: 'top-right',
          duration: 9000,
          isClosable: true,
        })
        onScheduleCreated()

        onClose()
      } catch (error) {
        toast({
          title: "スケジュールが作成されませんでした。",
          description: "スケジュールの作成に失敗しました。もう一度お試しください。",
          status: 'error',
          position: 'top-right',
          duration: 9000,
          isClosable: true,
        })
      }
    } 
  }
  
  return (
    <Modal
      isOpen={isOpen}
      onClose={onClose}
      size={'xl'}
    >
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>新しいスケジュールを追加する</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <FormControl mb={4}>
            <FormLabel>教師</FormLabel>
            <TeachersSelector value={selectedTeacher} onChange={setSelectedTeacher} />
          </FormControl>
          <FormControl mb={4} isInvalid={errors['students']}>
            <FormLabel>児童</FormLabel>
            <StudentsSelector isMulti value={selectedStudents} onChange={setSelectedStudents} />
            {errors['students'] === true && <FormErrorMessage>児童は最大3人まで選択できます。</FormErrorMessage>}
          </FormControl>
          <FormControl mb={4} isInvalid={errors['lessons']}>
            <FormLabel>科目</FormLabel>
            <LessonOptionsSelector isMulti value={selectedLessons} onChange={setSelectedLessons} />
            {errors['lessons'] === true && <FormErrorMessage>科目は最大2つまで選択できます。</FormErrorMessage>}
          </FormControl>
        </ModalBody>
        <ModalFooter>
          <Button 
            isLoading={isLoading}
            loadingText='Submitting'
            mt={4} 
            mr={3} 
            colorScheme="teal" 
            onClick={handleSubmit} 
            isDisabled={selectedTeacher === null || selectedStudents === null || selectedLessons === null}
          >
            追加
          </Button>
          <Button mt={4} onClick={onClose} isLoading={isLoading}>キャンセル</Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}

export default CreateScheduleModal