import { useEffect } from "react";
import { fetchLessons } from "../redux/actions/lessonActions";
import { useAppDispatch, useAppSelector } from "../redux/hooks";
import { Lesson } from "../types";
import { Select } from "chakra-react-select";
import { lessonsNamesMap } from '../const';

interface LessonOptionsSelectorProps {
  isDisabled?: boolean
  isMulti?: boolean
  value: Lesson[]
  onChange: (lessons: Lesson[]) => void
}

const LessonOptionsSelector = ({ isDisabled=false, isMulti = false, value, onChange }: LessonOptionsSelectorProps) => {
  const dispatch = useAppDispatch()
  const lessons = useAppSelector((state) => state.lessons?.lessons)
  // @ts-ignore
  const options = lessons.map((lesson: any) => ({ value: lesson.id, label: lessonsNamesMap[lesson.name] }));

  useEffect(() => {
    dispatch(fetchLessons())
  }, [dispatch])

  const handleChangeSelector = (selectedOptions: any) => {
    if (isMulti) {
      onChange(selectedOptions)
    } else {
      onChange([selectedOptions])
    }
  }

  return (
    <Select
      isDisabled={isDisabled}
      isMulti={isMulti}
      options={options}
      onChange={handleChangeSelector}
      value={value}
      placeholder={isMulti ? "科目を選択してください（最大2つ）" : "科目を選択してください"}
      chakraStyles={{
        control: (provided) => ({
          ...provided,
          colorScheme: 'teal',
        }),
      }}
    />
  )
}

export default LessonOptionsSelector