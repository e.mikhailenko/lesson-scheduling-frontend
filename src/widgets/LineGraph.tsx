import { useColorMode, useTheme } from "@chakra-ui/react";
import { useStatisticData } from "../hooks/use-statistic-data"
import { useEffect } from "react";
import { Chart, registerables } from 'chart.js';
import { Bar } from "react-chartjs-2";
import { TeachersWorkingHourPerWeek } from "../types";

Chart.register(...registerables);

const LineGraphWorkingHours = () => {
  const { colorMode } = useColorMode();
  const theme = useTheme();
  const { teachersWorkingOurPerWeek } = useStatisticData()

  useEffect(() => {
    Chart.defaults.color = colorMode === 'dark' ? theme.colors.whiteAlpha[900] : theme.colors.gray[900];
    Chart.defaults.borderColor = colorMode === 'dark' ? theme.colors.whiteAlpha[700] : theme.colors.gray[300];
  }, [colorMode, theme])

  const labels = ['月', '火', '水', '木', '金'];

  const getDayOfWeek: { [key: string]: string } = {
    '月': 'monday', 
    '火': 'tuesday',
    '水': 'wednesday',
    '木': 'thursday',
    '金': 'friday', 
  }

  const datasets = Object.values(teachersWorkingOurPerWeek).map((teacher: TeachersWorkingHourPerWeek) => ({
    label: teacher.name,
    data: labels.map(day => teacher.workingHours[getDayOfWeek[day.toLowerCase()]] || 0),
    backgroundColor: theme.colors.blue[200],
    borderColor: theme.colors.blue[500],
    borderWidth: 1,
  }));

  const data = {
    labels,
    datasets,
  };

  const options = {
    plugins: {
      title: {
        display: true,
        text: '時間/日',
      },
      colors: {
        forceOverride: true
      },
    },
    scales: {
      y: {
        beginAtZero: true,
        grid: {
          color: colorMode === 'dark' ? theme.colors.whiteAlpha[300] : theme.colors.gray[200],
        },
      },
      x: {
        grid: {
          color: colorMode === 'dark' ? theme.colors.whiteAlpha[300] : theme.colors.gray[200],
        },
      },
    },
  }  
  
  return <Bar data={data} options={options} />
}

export default LineGraphWorkingHours