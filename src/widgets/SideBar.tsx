import { Box, Button, ChakraProps, Flex, Text } from "@chakra-ui/react"
import NavItem from "../components/NavItem"
import { NavLink } from "react-router-dom"
import { DetailedHTMLProps, HTMLAttributes } from "react"
import { JSX } from "react/jsx-runtime"

const SideBar = (props: JSX.IntrinsicAttributes & Omit<DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>, "_hover" | "_active" | "_focus" | "_highlighted" | "_focusWithin" | "_focusVisible" | "_disabled" | "_readOnly" | "_before" | "_after" | "_empty" | "_expanded" | "_checked" | "_grabbed" | "_pressed" | "_invalid" | "_valid" | "_loading" | "_selected" | "_hidden" | "_autofill" | "_even" | "_odd" | "_first" | "_firstLetter" | "_last" | "_notFirst" | "_notLast" | "_visited" | "_activeLink" | "_activeStep" | "_indeterminate" | "_groupHover" | "_peerHover" | "_groupFocus" | "_peerFocus" | "_groupFocusVisible" | "_peerFocusVisible" | "_groupActive" | "_peerActive" | "_groupDisabled" | "_peerDisabled" | "_groupInvalid" | "_peerInvalid" | "_groupChecked" | "_peerChecked" | "_groupFocusWithin" | "_peerFocusWithin" | "_peerPlaceholderShown" | "_placeholder" | "_placeholderShown" | "_fullScreen" | "_selection" | "_rtl" | "_ltr" | "_mediaDark" | "_mediaReduceMotion" | "_dark" | "_light" | "_horizontal" | "_vertical" | "alignContent" | "alignItems" | "alignSelf" | "appearance" | "aspectRatio" | "backdropFilter" | "backgroundAttachment" | "backgroundBlendMode" | "backgroundClip" | "backgroundColor" | "backgroundImage" | "backgroundRepeat" | "backgroundSize" | "blockSize" | "borderBlockEndColor" | "borderBlockEndStyle" | "borderBlockEndWidth" | "borderBlockStartColor" | "borderBlockStartStyle" | "borderBlockStartWidth" | "borderBottomColor" | "borderBottomLeftRadius" | "borderBottomRightRadius" | "borderBottomStyle" | "borderBottomWidth" | "borderEndEndRadius" | "borderEndStartRadius" | "borderInlineEndColor" | "borderInlineEndStyle" | "borderInlineEndWidth" | "borderInlineStartColor" | "borderInlineStartStyle" | "borderInlineStartWidth" | "borderLeftColor" | "borderLeftStyle" | "borderLeftWidth" | "borderRightColor" | "borderRightStyle" | "borderRightWidth" | "borderStartEndRadius" | "borderStartStartRadius" | "borderTopColor" | "borderTopLeftRadius" | "borderTopRightRadius" | "borderTopStyle" | "borderTopWidth" | "bottom" | "boxDecorationBreak" | "boxShadow" | "boxSizing" | "clipPath" | "color" | "columnGap" | "cursor" | "display" | "filter" | "flexBasis" | "flexDirection" | "flexGrow" | "flexShrink" | "flexWrap" | "float" | "fontFamily" | "fontSize" | "fontStyle" | "fontWeight" | "gridAutoColumns" | "gridAutoFlow" | "gridAutoRows" | "gridColumnEnd" | "gridColumnStart" | "gridRowEnd" | "gridRowStart" | "gridTemplateAreas" | "gridTemplateColumns" | "gridTemplateRows" | "height" | "inlineSize" | "insetBlockEnd" | "insetBlockStart" | "insetInlineEnd" | "insetInlineStart" | "isolation" | "justifyContent" | "justifyItems" | "justifySelf" | "left" | "letterSpacing" | "lineHeight" | "listStyleImage" | "listStylePosition" | "listStyleType" | "marginBlockEnd" | "marginBlockStart" | "marginBottom" | "marginInlineEnd" | "marginInlineStart" | "marginLeft" | "marginRight" | "marginTop" | "maxBlockSize" | "maxHeight" | "maxInlineSize" | "maxWidth" | "minBlockSize" | "minHeight" | "minInlineSize" | "minWidth" | "mixBlendMode" | "objectFit" | "objectPosition" | "opacity" | "order" | "outlineColor" | "outlineOffset" | "overflowWrap" | "overflowX" | "overflowY" | "overscrollBehaviorX" | "overscrollBehaviorY" | "paddingBlockEnd" | "paddingBlockStart" | "paddingBottom" | "paddingInlineEnd" | "paddingInlineStart" | "paddingLeft" | "paddingRight" | "paddingTop" | "pointerEvents" | "position" | "resize" | "right" | "rotate" | "rowGap" | "scale" | "scrollBehavior" | "scrollMarginBottom" | "scrollMarginLeft" | "scrollMarginRight" | "scrollMarginTop" | "scrollPaddingBottom" | "scrollPaddingLeft" | "scrollPaddingRight" | "scrollPaddingTop" | "scrollSnapAlign" | "scrollSnapStop" | "scrollSnapType" | "textAlign" | "textDecorationColor" | "textDecorationLine" | "textDecorationStyle" | "textDecorationThickness" | "textIndent" | "textOverflow" | "textShadow" | "textTransform" | "textUnderlineOffset" | "top" | "transform" | "transformOrigin" | "transitionDelay" | "transitionDuration" | "transitionProperty" | "transitionTimingFunction" | "translate" | "userSelect" | "verticalAlign" | "visibility" | "whiteSpace" | "width" | "willChange" | "wordBreak" | "zIndex" | "animation" | "background" | "backgroundPosition" | "border" | "borderBlock" | "borderBlockEnd" | "borderBlockStart" | "borderBottom" | "borderColor" | "borderInline" | "borderInlineEnd" | "borderInlineStart" | "borderLeft" | "borderRadius" | "borderRight" | "borderStyle" | "borderTop" | "borderWidth" | "flex" | "flexFlow" | "gap" | "gridArea" | "gridColumn" | "gridRow" | "gridTemplate" | "inset" | "insetBlock" | "insetInline" | "margin" | "marginBlock" | "marginInline" | "outline" | "overflow" | "overscrollBehavior" | "padding" | "paddingBlock" | "paddingInline" | "placeContent" | "placeItems" | "placeSelf" | "scrollMargin" | "scrollPadding" | "textDecoration" | "transition" | "gridColumnGap" | "gridGap" | "gridRowGap" | "fill" | "stroke" | "m" | "mt" | "mr" | "marginEnd" | "me" | "mb" | "ml" | "marginStart" | "ms" | "mx" | "marginX" | "my" | "marginY" | "p" | "pt" | "pr" | "paddingEnd" | "pe" | "pb" | "pl" | "paddingStart" | "ps" | "px" | "paddingX" | "py" | "paddingY" | "textColor" | "noOfLines" | "isTruncated" | "flexDir" | "translateX" | "translateY" | "skewX" | "skewY" | "scaleX" | "scaleY" | "blur" | "brightness" | "contrast" | "hueRotate" | "invert" | "saturate" | "dropShadow" | "backdropBlur" | "backdropBrightness" | "backdropContrast" | "backdropHueRotate" | "backdropInvert" | "backdropSaturate" | "hideFrom" | "hideBelow" | "w" | "boxSize" | "maxW" | "minW" | "h" | "maxH" | "minH" | "overscroll" | "overscrollX" | "overscrollY" | "rounded" | "borderStartWidth" | "borderEndWidth" | "borderStartStyle" | "borderEndStyle" | "borderStartColor" | "borderEndColor" | "borderEnd" | "borderStart" | "borderTopRadius" | "roundedTop" | "borderRightRadius" | "roundedRight" | "roundedEnd" | "borderInlineEndRadius" | "borderEndRadius" | "borderBottomRadius" | "roundedBottom" | "borderLeftRadius" | "roundedLeft" | "roundedStart" | "borderInlineStartRadius" | "borderStartRadius" | "borderTopStartRadius" | "roundedTopLeft" | "roundedTopStart" | "borderTopEndRadius" | "roundedTopRight" | "roundedTopEnd" | "borderBottomStartRadius" | "roundedBottomLeft" | "roundedBottomStart" | "borderBottomEndRadius" | "roundedBottomRight" | "roundedBottomEnd" | "borderX" | "borderY" | "shadow" | "blendMode" | "bgBlendMode" | "bg" | "bgClip" | "bgColor" | "bgGradient" | "bgPos" | "bgImage" | "bgImg" | "bgRepeat" | "bgSize" | "bgAttachment" | "bgPosition" | "listStylePos" | "listStyleImg" | "insetEnd" | "insetStart" | "insetX" | "insetY" | "pos" | "ring" | "ringColor" | "ringOffset" | "ringOffsetColor" | "ringInset" | "scrollMarginX" | "scrollMarginY" | "scrollPaddingX" | "scrollPaddingY" | "textDecor" | "srOnly" | "layerStyle" | "textStyle" | "apply" | "__css" | "sx" | "css" | "as"> & { htmlTranslate?: "yes" | "no" | undefined } & Omit<ChakraProps, never> & { as?: "div" | undefined }) => {
  return (
    <Box
      as="nav"
      pos="fixed"
      top="0"
      left="0"
      zIndex="sticky"
      h="full"
      pb="10"
      overflowX="hidden"
      overflowY="auto"
      color="inherit"
      borderRightWidth="1px"
      _dark={{
        bg: "gray.800",
      }}
      bg="white"
      w="60"
      {...props}
    >
      <Flex px="4" py="5" align="center">

        <Text
          fontSize="m"
          ml="2"
          color="brand.500"
          _dark={{
            color: "white",
          }}
          fontWeight="semibold"
        >
          学校のスケジューラー
        </Text>
      </Flex>
      <Flex
        direction="column"
        as="nav"
        fontSize="sm"
        color="gray.600"
        aria-label="Main Navigation"
      >
        <NavItem children={
          <Button as={NavLink} to="/schedule" colorScheme="teal" variant="ghost">
            スケジュール 
          </Button>
        } />
        <NavItem children={
          <Button as={NavLink} to="/students" colorScheme="teal" variant="ghost">
            児童
          </Button>
        } />
        <NavItem children={
          <Button as={NavLink} to="/teachers" colorScheme="teal" variant="ghost">
            教師
          </Button>
        } />
        <NavItem children={
          <Button as={NavLink} to="/statistics" colorScheme="teal" variant="ghost">
            統計
          </Button>
        } />
      </Flex>
    </Box>
  )
}

export default SideBar