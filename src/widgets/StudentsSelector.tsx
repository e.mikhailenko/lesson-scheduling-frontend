import { useAppDispatch, useAppSelector } from "../redux/hooks"
import { Student } from "../types"
import { fetchStudents } from "../redux/actions/studentActions"
import { useEffect } from "react"
import { Select } from "chakra-react-select"

interface StudentsSelectorProps {
  isMulti?: boolean
  value: Student[]
  onChange: (students: Student[]) => void
}

const StudentsSelector = ({ isMulti = false, value, onChange }: StudentsSelectorProps) => {
  const dispatch = useAppDispatch()
  const students = useAppSelector((state) => state.students?.students)
  const options = students.map((student: any) => ({ value: student.id, label: student.name }))

  useEffect(() => {
    dispatch(fetchStudents())
  }, [dispatch])

  const handleChangeSelector = (selectedOptions: any) => {
    if (isMulti) {
      onChange(selectedOptions)
    } else {
      onChange([selectedOptions])
    }
  }

  return (
    <Select
      isMulti={isMulti}
      options={options}
      onChange={handleChangeSelector}
      value={value}
      placeholder={isMulti ? "児童を選択してください（最大3人）" : "児童を選択してください"}
      chakraStyles={{
        control: (provided) => ({
          ...provided,
          colorScheme: 'teal',
        }),
      }}
    />
  )
}

export default StudentsSelector