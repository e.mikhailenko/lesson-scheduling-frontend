import { Box, useColorMode, useTheme } from "@chakra-ui/react";
import { useStatisticData } from "../hooks/use-statistic-data";
import { Chart, Colors, registerables } from "chart.js";
import { useEffect } from "react";
import { Pie } from "react-chartjs-2";

Chart.register(...registerables, Colors);

const TeacherTotalWorkingHoursPieChart = () => {
  const { colorMode } = useColorMode()
  const theme = useTheme()
  const { teacherTotalWorkingHours } = useStatisticData()

  useEffect(() => {
    Chart.defaults.color = colorMode === 'dark' ? theme.colors.whiteAlpha[900] : theme.colors.gray[900];
    Chart.defaults.borderColor = colorMode === 'dark' ? theme.colors.whiteAlpha[700] : theme.colors.gray[300];
  }, [colorMode, theme])

  const chartData = {
    labels: teacherTotalWorkingHours.map(teacher => teacher.name),
    datasets: [{
      data: teacherTotalWorkingHours.map(teacher => teacher.hours),
    }]
  }

  const options = {
    plugins: {
      title: {
        display: true,
        text: '時間/週',
      },
      colors: {
        forceOverride: true
      },
      legend: {
        display: true,
        position: "right" as const,
        labels: {
          color: colorMode === 'dark' ? theme.colors.whiteAlpha[900] : theme.colors.gray[900],
        }
      }
    }
  }

  return (
    <Box width="400px" height="400px">
      <Pie data={chartData} options={options} />
    </Box>
  )
}

export default TeacherTotalWorkingHoursPieChart