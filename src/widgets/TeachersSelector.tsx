import { useSelector } from "react-redux"
import { useAppDispatch, useAppSelector } from "../redux/hooks"
import { Teacher } from "../types"
import { fetchTeachers } from "../redux/actions/teacherActions"
import { Select } from "chakra-react-select"
import { useEffect } from "react"

interface TeachersSelectorProps {
  isMulti?: boolean
  value: Teacher[]
  onChange: (teachers: Teacher[]) => void
}

const TeachersSelector = ({ isMulti = false, value, onChange }: TeachersSelectorProps) => {
  const dispatch = useAppDispatch()
  const teachers = useAppSelector((state) => state.teachers?.teachers)
  const options = teachers?.map((teacher: Teacher) => ({ value: teacher.id, label: teacher.name }))

  useEffect(() => {
    dispatch(fetchTeachers())
  }, [dispatch])

  const handleChangeSelector = (selectedOptions: any) => {
    if (isMulti) {
      onChange(selectedOptions)
    } else {
      onChange([selectedOptions])
    }
  }

  return (
    <Select
      isMulti={isMulti}
      options={options}
      onChange={handleChangeSelector}
      value={value}
      placeholder={"教師を選択"}
      chakraStyles={{
        control: (provided) => ({
          ...provided,
          colorScheme: 'teal',
        }),
      }}
    />
  )
}

export default TeachersSelector